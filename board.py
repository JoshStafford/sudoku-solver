import numpy as np
from math import sqrt, ceil

board = [[0, 0, 0, 7, 9, 0, 0, 5, 0],
         [3, 5, 2, 0, 0, 8, 0, 4, 0],
         [0, 0, 0, 0, 0, 0, 0, 8, 0],
         [0, 1, 0, 0, 7, 0, 0, 0, 4],
         [6, 0, 0, 3, 0, 1, 0, 0, 8],
         [9, 0, 0, 0, 8, 0, 0, 1, 0],
         [0, 2, 0, 0, 0, 0, 0, 0, 0],
         [0, 4, 0, 5, 0, 0, 8, 9, 1],
         [0, 8, 0, 0, 3, 7, 0, 0, 0]]

board1 = [[5, 3, 0, 0, 7, 0, 0, 0, 0],
          [6, 0, 0, 1, 9, 5, 0, 0, 0],
          [0, 9, 8, 0, 0, 0, 0, 6, 0],
          [8, 0, 0, 0, 6, 0, 0, 0, 3],
          [4, 0, 0, 8, 0, 3, 0, 0, 1],
          [7, 0, 0, 0, 2, 0, 0, 0, 6],
          [0, 6, 0, 0, 0, 0, 2, 8, 0],
          [0, 0, 0, 4, 1, 9, 0, 0, 5],
          [0, 0, 0, 0, 8, 0, 0, 7, 9]]

class Board:

    def __init__(this, board):
        this.board = board
        this.rows = len(this.board)
        this.cols = len(this.board[0])

    def __repr__(this):
        return str(np.matrix(this.board))

    @staticmethod
    def square_start(val):
        return val - (val%3)

    @staticmethod
    def __solve(board, x=0, y=0):

        next_x = (x+1)%len(board[0])
        next_y = y
        if(x+1 >= 9):
            next_y += 1

        if(y >= len(board)):
            return board

        if(board[y][x] != 0):
            return Board.__solve(board, next_x, next_y)

        for i in range(1, 10):

            if(Board.validate(board, x, y, i)):
                board[y][x] = i
                b = Board.__solve(board, next_x, next_y)
                if(b is not None):
                    return b

        board[y][x] = 0
        return None

    # Pass in board in matrix form
    # Board object returned
    @staticmethod
    def solver(b):

        solved = Board.__solve(b)
        return Board(solved)

    @staticmethod
    def validate(board, x, y, n):

        grid_square_x = Board.square_start(x)
        grid_square_y = Board.square_start(y)

        # Check row
        for val in board[y]:
            if(val == n):
                # print("Row")
                return False

        # Check column
        for i in range(len(board)):
            if(board[i][x] == n):
                # print("Column")
                return False

        # Check grid square
        for i in range(grid_square_y, grid_square_y+3):

            for j in range(grid_square_x, grid_square_x+3):

                if(board[i][j] == n):
                    # print("Square")
                    return False

        # Value not found, move valid
        return True
